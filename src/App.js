import { useState } from 'react';
import axios from 'axios';
import FormData from 'form-data';
import 'antd/dist/antd.css';
import {
  Button,
  Form,
  Input,
  Select,
  Upload,
} from 'antd';
import { UploadOutlined } from '@ant-design/icons';

const { Option } = Select;

function App() {
  const [submitting, setSubmitting] = useState(false);
  const [fileList, setFileList] = useState([]);

  const components = [];
  for (let i = 1; i <= 20; i++) {
    components.push({
      component_id: `${i}`,
      label: `Component ${i}`,
    });
  }

  const handleSubmit = (values) => {
    console.log('Received values of form: ', values);

    setSubmitting(true);

    const data = new FormData();

    for (const key of Object.keys(values)) {
      console.log(key, values[key]);

      if (values[key] && values[key].hasOwnProperty('file')) {
        values[key] = values[key].file;
      }

      if (values[key] && Array.isArray(values[key])) {
        if (key === 'header_left' || key === 'header_right') {
          values[key] = components.filter(item => {
            return values[key].includes(item.component_id);
          });
        }

        values[key] = JSON.stringify(values[key]);
      }

      data.append(key, values[key]);
    }

    const options = {
      method: 'POST',
      url: 'http://localhost:8888/payslip/template',
      headers: {
        'x-scope': 'demo_calculator',
        'x-user-id': 1,
        'Content-Type': 'multipart/form-data',
      },
      data,
    };

    axios(options).then(response => {
      console.log(response.data);
    }).catch(error => {
      console.log(error);
    });

    setSubmitting(false);
  };

  const props = {
    onRemove: () => {
      setFileList([]);
    },
    beforeUpload: file => {
      setFileList([file]);
      return false;
    },
    fileList,
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 14,
    },
  };

  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 14,
    },
  };

  const children = [];
  for (const index of Object.keys(components)) {
    children.push(<Option key={components[index].component_id}>{components[index].label}</Option>);
  }

  function handleChange(value) {
    console.log(`selected ${value}`);
  }

  return (
    <>
      <Form
        {...layout}
        initialValues={{
          name: 'Template Dummy',
          sequence_id: 1,
          divisi: 1,
          company_name: 'Dummy',
          company_address: 'Indonesia',
          company_contact: '911',
          display_information: 'Catatan',
          earnings_label: 'Pendapatan',
          deductions_label: 'Potongan',
          header_left: ['1', '2'],
          header_right: ['1', '2'],
          earnings: ['1', '2'],
          deductions: ['1', '2'],
          additionals: ['1', '2'],
        }}
        onFinish={handleSubmit}
      >
        <Form.Item
          label="Name"
          name="name"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Sequence Id"
          name="sequence_id"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Divisi"
          name="divisi"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Banner Image"
          name="banner_image"
        >
          <Upload {...props}>
            <Button icon={<UploadOutlined />}>Select File</Button>
          </Upload>
        </Form.Item>
        <Form.Item
          label="Company Name"
          name="company_name"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Company Address"
          name="company_address"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Company Contact"
          name="company_contact"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Display Information"
          name="display_information"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Earnings Label"
          name="earnings_label"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Deductions Label"
          name="deductions_label"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Header Left"
          name="header_left"
        >
          <Select
            mode="multiple"
            allowClear
            style={{ width: '100%' }}
            placeholder="Please select"
            onChange={handleChange}
          >
            {children}
          </Select>
        </Form.Item>
        <Form.Item
          label="Header Right"
          name="header_right"
        >
          <Select
            mode="multiple"
            allowClear
            style={{ width: '100%' }}
            placeholder="Please select"
            onChange={handleChange}
          >
            {children}
          </Select>
        </Form.Item>
        <Form.Item
          label="Earnings"
          name="earnings"
        >
          <Select
            mode="multiple"
            allowClear
            style={{ width: '100%' }}
            placeholder="Please select"
            onChange={handleChange}
          >
            {children}
          </Select>
        </Form.Item>
        <Form.Item
          label="Deductions"
          name="deductions"
        >
          <Select
            mode="multiple"
            allowClear
            style={{ width: '100%' }}
            placeholder="Please select"
            onChange={handleChange}
          >
            {children}
          </Select>
        </Form.Item>
        <Form.Item
          label="Additionals"
          name="additionals"
        >
          <Select
            mode="multiple"
            allowClear
            style={{ width: '100%' }}
            placeholder="Please select"
            onChange={handleChange}
          >
            {children}
          </Select>
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button
            type="primary"
            htmlType="submit"
            disabled={fileList.length === 0}
            loading={submitting}
          >
            {submitting ? 'Submitting' : 'Submit'}
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}

export default App;
